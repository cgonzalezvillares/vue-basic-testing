## Repository
https://bitbucket.org/cgonzalezvillares/vue-basic-testing

## CI
For all branches

[Codeship](https://app.codeship.com/projects/450371)


## Deployment

[Heroku](https://dashboard.heroku.com/apps/vue-basic-testing)
### QA  
Automatic deploy on master updates  

https://vue-basic-testing.herokuapp.com